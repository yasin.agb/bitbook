@include('layouts.user.partials.header')

<body class="page-container-bg-solid page-md">
<div class="page-wrapper">
    <div class="page-wrapper-row">
        <div class="page-wrapper-top">
            <!-- BEGIN HEADER -->
                @include('layouts.user.partials.nav')
            <!-- END HEADER -->
        </div>
    </div>
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    @yield('content')
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</div>

@include('layouts.user.partials.footer')